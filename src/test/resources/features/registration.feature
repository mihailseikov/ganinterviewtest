Feature: Registration

  Scenario: User should not be able to register without entering date of birth

    Given the browser is open
    And the user is on the home page
    When the user clicks on Join Now button
    And the user selects title
    And the user enters first and last name
    And the user accepts terms and conditions
    And the user clicks Join now to submit the form
    Then the user should see an error message under date of birth field
