package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class homePage {
    WebDriver driver;

    By joinNow = By.cssSelector("a[href=\"/sign-up.shtml\"]");

    public homePage(WebDriver driver){
        this.driver = driver;
    }

    public void clickJoinNow() {
        driver.findElement(this.joinNow).click();
    }
}
