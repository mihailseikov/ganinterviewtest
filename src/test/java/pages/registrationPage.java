package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class registrationPage {
    WebDriver driver;

    By title = By.id("title");
    By firstName = By.id("forename");
    By lastName = By.cssSelector("[name=\"map(lastName)\"]");
    By termsAndConditions = By.cssSelector("input[name=\"map(terms)\"]");
    By dateOfBirthError = By.cssSelector("label[for=\"dob\"]");
    By joinNow = By.id("form");

    public registrationPage (WebDriver driver) {
        this.driver = driver;
    }

    public void setTitle(String title) {
        driver.findElement(this.title).sendKeys(title);
    }

    public void setFirstName(String firstName) {
        driver.findElement(this.firstName).sendKeys(firstName);
    }

    public void setLastName(String lastName) {
        driver.findElement(this.lastName).sendKeys(lastName);
    }

    public void clickTermsAndConditions() {
        driver.findElement(this.termsAndConditions).click();
    }

    public void clickJoinNow(){
        driver.findElement(this.joinNow).click();
    }

    public void verifyDateOfBirthErrorMessage(String errorMessage) {
        driver.findElement(this.dateOfBirthError).isDisplayed();
        String errorText = driver.findElement(this.dateOfBirthError).getText();
        assert errorText.equals(errorMessage);
    }
}
