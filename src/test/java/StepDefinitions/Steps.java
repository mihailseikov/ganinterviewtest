package StepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.homePage;
import pages.registrationPage;

public class Steps {
    WebDriver driver;
    homePage home;
    registrationPage registration;

    @Given("the browser is open")
    public void the_browser_is_open() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
        driver = new ChromeDriver();
    }
    @And("the user is on the home page")
    public void the_user_is_on_the_home_page() {
        driver.get("https://moneygaming.qa.gameaccount.com");
    }
    @When("the user clicks on Join Now button")
    public void the_user_clicks_on_join_now_button() {
        home = new homePage(driver);

        home.clickJoinNow();
    }
    @And("the user selects title")
    public void the_user_selects_title() {
        registration = new registrationPage(driver);

        registration.setTitle("Mr");
    }
    @And("the user enters first and last name")
    public void the_user_enters_first_and_last_name() {
        registration.setFirstName("Mihail");
        registration.setLastName("Seykov");
    }
    @And("the user accepts terms and conditions")
    public void the_user_accepts_terms_and_conditions() {
      registration.clickTermsAndConditions();
    }
    @And("the user clicks Join now to submit the form")
    public void the_user_clicks_join_now_to_submit_the_form() {
       registration.clickJoinNow();
    }
    @Then("the user should see an error message under date of birth field")
    public void the_user_should_see_an_error_message_under_date_of_birth_field() {
        registration.verifyDateOfBirthErrorMessage("This field is required");

        driver.quit();
    }
}
